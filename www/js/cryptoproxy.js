angular.module('cryptoproxy', ['ConfigMaster'])

.factory('CryptoProxy', function(ConfigMaster) {
	var factory = {};

	factory.sharedSecret = ConfigMaster.crypto_sharedSecret;

	factory.getHash = function(stringContent) {
		var shaObj = new jsSHA("SHA-512", "TEXT");
		shaObj.setHMACKey(this.sharedSecret, "TEXT");
		shaObj.update(stringContent);
		var hmac = shaObj.getHMAC("B64");


		return hmac;
	}

	return factory;
});

