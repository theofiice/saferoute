angular.module('SpeechHandler', [])

.factory('SpeechHandler', function($q) {
  var factory = {};

  factory.speechRecognitionWait = 5000;
  factory.startListen = 0;
  factory.currentState = "idle";
  factory.timerHandle = 0;
  factory.speechRecognitionOptions = {
    language: "en-US",
    matches: 1,
    prompt: "",
    showPopup: true,
    showPartial: false
  };

  factory.synth = window.speechSynthesis;
  
  if(window.plugins) {
    if(window.plugins.speechRecognition) {
      console.log("window.plugins.speechRecognition");
      window.plugins.speechRecognition.requestPermission(
        function(){
          // Success
          console.log("authorized ok");
        },
        function(){
          // Failure
          console.log("Nope");
        }
      );
    }
  }

  factory.say = function(textToSay) {
    var myRef = this;

    console.log("Saying: ", textToSay);

    console.log(window.TTS);

    return $q(function(resolve, reject) {
      var utterThis = new SpeechSynthesisUtterance(textToSay);
      utterThis.pitch = 1;
      utterThis.rate = 1;
      myRef.synth.speak(utterThis);
    });
  }

  factory.stopListening = function() {
    console.log("Stopping the listeningz");
    var myRef = this;
    console.log("myRef.currentState: ", myRef.currentState);
    // if(myRef.currentState != "idle") {
      window.plugins.speechRecognition.stopListening(
        function(){
          console.log("Successfully stopped listening");
          myRef.currentState = "idle";
        },
        function(x){
          console.log("Couldn't stop listening.", x);
          myRef.currentState = "idle";
        }
      );
      
    // }
  }

  factory.listen = function() {
    var myRef = this;

    return $q(function(resolve, reject) {
      myRef.currentState = "listening";
      window.plugins.speechRecognition.startListening(
        function(arrayOfMatches){
          // Success
          myRef.currentState = "idle";
          console.log("Got words back.");
          resolve(arrayOfMatches[0].toString().toLowerCase());
        }, 
        function(){
          // fail
          reject();
        }, 
        myRef.speechRecognitionOptions
      );
      myRef.startListen = new Date();
      factory.timerHandle = setTimeout(
        myRef.stopListening,
        myRef.speechRecognitionWait
      );
    });
  }
  
  return factory;
});
