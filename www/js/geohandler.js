angular.module('GeoHandler', [])

.factory('GeoHandler', function($q) {
	var factory = {};

	factory.defaultOptions = { maximumAge: 10000, timeout: 5000, enableHighAccuracy: true };

	factory.monitorHandle = 0;

	factory.defaultMonitorInterval = 10000;

	factory.getCurrentPosition = function() {
		var myRef = this;
		return $q(function(resolve, reject) {
			navigator.geolocation.getCurrentPosition(resolve, reject, myRef.defaultOptions);
		});
	}

	factory.checkLocation = function() {
		var myRef = this;

		myRef.getCurrentPosition().then(function(position){
			var lat = position.coords.latitude;
			var lon = position.coords.longitude;

			// Check to see if there is danger nearby
			ChavaWebProxy.getIncidents(false).then(function(res) {
				res = res.replace("\n", "");
				myRef.incidents = JSON.parse(res);
				
			});

		});
	}

	factory.monitorProgress = function() {
		var myRef = this;
		return $q(function(resolve, reject) {
			myRef.getCurrentPosition().then(function(){
				// Warming up the oven - LOL
			});
			myRef.monitorHandle = setInterval(myRef.checkLocation, myRef.defaultMonitorInterval);
			resolve();
		});
	}

	factory.stopMonitoring = function() {
		var myRef = this;
		clearInterval(myRef.monitorHandle);
	}

	return factory;
});