function CustomMarker(latlng, map, args) {
	this.latlng = latlng;	
	this.args = args;	
	this.setMap(map);	
}

function locObj() {
        this._lat;
        this._lng;
        this.lat = function() {
          return this._lat;
        }
        this.lng = function() {
          return this._lng;
        }
      }

function initMap() {
	window.CustomMarker.prototype = new google.maps.OverlayView();

	window.CustomMarker.prototype.draw = function() {
		
		var self = this;
		
		var div = this.div;
		
		if (!div) {
		
			div = this.div = document.createElement('div');
			
			div.className = 'marker';
			
			div.style.position = 'absolute';
			div.style.cursor = 'pointer';
			div.style.width = '3em';
			div.style.height = '3em';
			div.style.borderRadius = '25%';
			div.style.background = 'white';
			div.style.textAlign = "center";
			
			if (typeof(self.args.marker_id) !== 'undefined') {
				div.dataset.marker_id = self.args.marker_id;
			}

			if (typeof(self.args.icon_class) !== 'undefined') {
				div.innerHTML = "<i class='" + self.args.icon_class + " fgTeal' style='font-size: 3em;'></i>";
			}
			
			google.maps.event.addDomListener(div, "click", function(event) {
				// alert('You clicked on a custom marker!');			
				google.maps.event.trigger(self, "click");
			});
			
			var panes = this.getPanes();
			panes.overlayImage.appendChild(div);
		}
		
		var point = this.getProjection().fromLatLngToDivPixel(this.latlng);
		
		if (point) {
			div.style.left = (point.x - 10) + 'px';
			div.style.top = (point.y - 20) + 'px';
		}
	};

	window.CustomMarker.prototype.remove = function() {
		if (this.div) {
			this.div.parentNode.removeChild(this.div);
			this.div = null;
		}	
	};

	window.CustomMarker.prototype.hide = function() {
		if (this.div) {
			this.div.style.visibility = "hidden";
			// // console.log("hide");
		}	
	};

	window.CustomMarker.prototype.show = function() {
		if (this.div) {
			this.div.style.visibility = "inherit";
			// // console.log("show");
		}	
	};

	window.CustomMarker.prototype.getPosition = function() {
		return this.latlng;	
	};
}