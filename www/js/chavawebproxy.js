angular.module('ChavaWebProxy', [])

.factory('ChavaWebProxy', function($http, $q, ConfigMaster, CryptoProxy, $ionicPlatform, $rootScope) {
	var factory = {};
	factory.storageKey = ConfigMaster.local_storageKey;
	factory.session = {};
	factory.urlBase = ConfigMaster.chavaweb_api;
	factory.authRef = {notAuthorized: true};
	factory.authObject = {};

	factory.p2pHandles = {};
	factory.forumHandles = {};
	factory.announceHandles = {};

	factory.genHeaders = function(aK) {
	  var auth = "";
	  if(aK) auth = aK;

	  var data = localStorage.getItem(ConfigMaster.local_storageKey + "." + "authKey");
	  var parsedData;
	  try {
	    parsedData = JSON.parse(data);
	  }
	  catch(ex){
	    parsedData = null;
	  }
	  auth = parsedData;

	  var h = {headers: { 
	    "authKey": auth, 
	  }};

	  return h;
	}

	factory.getAuthKey = function() {
		var ref = this;
		var authKey = ref.retrieveData("authKey");
		ref.authKey = authKey;
	}

	factory.errorHandler = function(e) {
		console.log(e);
	}

	factory.generateAuth = function(u, p, saveAuth) {
		// var clientId = ConfigMaster.firebase_clientId;
		var h = CryptoProxy.getHash(u + ":" + p);
		if(saveAuth) {
			this.currentAuth = h;
		}
		return h;
	}



	factory.getIncidents = function(fakeIt) {
		var ref = this;
		var baseUrl = ConfigMaster.chavaweb_api;
		baseUrl += "route/incidents/" + fakeIt;
		return $q(function(resolve, reject) {
			$http.get(baseUrl, ref.genHeaders()).then(function(data){
				// console.log(data.data);
				resolve(data.data);
			});
		})
	}


	factory.storeData = function(subkey, data) {
		localStorage.setItem(this.storageKey + "." + subkey, JSON.stringify(data));
	}

	factory.removeData = function(subkey, data) {
		// localStorage.setItem(this.storageKey + "." + subkey, null);

		try {
			localStorage.removeItem(this.storageKey + "." + subkey);
		}
		catch(ex) {
			
		}
	}

	factory.removeAllData = function() {
		var ref = this;
		
		ref.removeData("Profile");
		ref.removeData("authKey");
		ref.removeData("authObject");
		ref.removeData("userName");
		ref.removeData("FBLogin");
		ref.removeData("FBProfile");
		ref.removeData("LoginStatus");
		ref.removeData("RegisterData");
		ref.removeData("CalendarEvents");
		ref.removeData("Articles");
		ref.removeData("Dictionary");
		ref.removeData("Favorites");
		ref.removeData("ParentIndicator");
		ref.removeData("ParentingTweet");
		ref.removeData("PlanningTweet");
		ref.removeData("PregnantIndicator");
		ref.removeData("Resources");
	}

	factory.retrieveData = function(subkey) {
		var data = localStorage.getItem(this.storageKey + "." + subkey);
		var parsedData;
		try {
			parsedData = JSON.parse(data);
		}
		catch(ex){
			parsedData = null;
		}
		// console.log(parsedData);
		return parsedData;
	}

	// Initialization script
	function doInit() {
		factory.getAuthKey();
	}

	doInit();

	window.fbproxy = factory;

	return factory;
});