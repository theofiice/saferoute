angular.module('starter.controllers')
.controller('MapCtrl', function($scope, $ionicPopup, ChavaWebProxy, ConfigMaster, SpeechHandler, GeoHandler, $ionicLoading) {


	var self = this;

	$scope.init = function() {
		// CHECK YOSELF
		$scope.geolocationOptions = {
		maximumAge: 3000,
		timeout: 5000,
		enableHighAccuracy: true
		};
	    $scope.resources = [];
	    $scope.holdingTank = [];
	    $scope.infoWindows = [];
	    

	    $scope.initMap();
	}

	$scope.initMap = function() {
	    $scope.map = new google.maps.Map(document.getElementById('resourcemap'), {
	      center: {lat: 39.770253, lng: -86.156640},
	      scrollwheel: false,
	      zoom: 11,
	      disableDefaultUI: true
	    });

	    $scope.geolocationOptions = {
	      maximumAge: 3000,
	      timeout: 5000,
	      enableHighAccuracy: true
	    };

	    navigator.geolocation.getCurrentPosition($scope.gotLocation,
	                                         $scope.failedLocation,
	                                         $scope.geolocationOptions);

	    // $scope.addResourcesToMap();

	}


  $scope.gotLocation = function(position) {
    var pos = {
      lat: position.coords.latitude,
      lng: position.coords.longitude
    };

    $scope.lastPos = pos;


	// FIND INCIDENTS
	ChavaWebProxy.getIncidents(false).then(function(res) {
		$scope.resources = JSON.parse(res).features;
		console.log($scope.resources);
		$scope.addResourcesToMap();
      	$ionicLoading.hide();
	});

    $scope.map.setCenter(pos);
  }


	$scope.failedLocation = function(error) {
		$ionicPopup.alert({
			title: "Where are you?",
			template: ConfigMaster.appName + " couldn't find your location. Please try again a bit later."
		});
		console.log(error);
	}

	$scope.doListen = function() {
		SpeechHandler.listen().then(function(results) {
			console.log(results);
		});
	}

	$scope.addResourcesToMap = function() {
		var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';



        // Create markers.
        $scope.resources.forEach(function(feature) {
        	console.log(feature)
        	feature.position = new google.maps.LatLng(feature.geometry.coordinates[1], feature.geometry.coordinates[0]);
			var marker = new google.maps.Marker({
				position: feature.position,
				icon: 'https://maps.gstatic.com/mapfiles/ms2/micons/caution.png',
				map: $scope.map
			});
        });


		// // $scope.clearMap();
		// var totalCount = 0;
		// console.log($scope.resources);
		// var infoWindowContentTemplate = document.getElementsByClassName("infoWindowTemplate")[0];
		// for (var i = 0; i < $scope.resources.length; i++) {
		// 	var resource = $scope.resources[i];
		// 	// console.log(resource)

		

		// 	var loc = new locObj();

		// 	loc._lat = resource.geometry.coordinates[0];
		// 	loc._lng = resource.geometry.coordinates[1];

		// 	var args = {
		// 	// marker_id: resource.$id,
		// 	colour: 'Red'
		// 	}

		// 	// Override for testing
		// 	args.icon_class = "ion-arrow-down-a";


		// 	var marker = new CustomMarker(
		// 		loc, 
		// 		$scope.map,
		// 		args
		// 	);

		// 	marker["idx"] = totalCount++;
		// 	// // console.log(resource);
		// 	// if(!!resource.Categories && !!resource.Categories[0].Id) marker["resourceType"] = resource.Categories[0].Id;
		// 	// if(!!infoWindowContentTemplate)var infoClone = infoWindowContentTemplate.cloneNode(true);
		// 	// infoClone.attributes.class.value = "infoWindowItem";

		//     // Create actual info window
		//       // var infoWindow = new InfoBubble({
		//       //   content: infoClone,
		//       //   disableAutoPan: true
		//       // });
		// 	// marker.addListener('click', function() {
		// 	// var infoWindow = $scope.infoWindows[this.idx];
		// 	// if($scope.activeInfoWindow != null) {
		// 	//   $scope.activeInfoWindow.close();
		// 	// }
		// 	// $scope.activeInfoWindow = infoWindow;
		// 	// infoWindow.open($scope.map, this);
		// 	// });

		// 	$scope.holdingTank.push(marker);
		// 	// $scope.infoWindows.push(infoWindow);
		// };
	}

	$scope.clearMap = function() {
		while($scope.holdingTank[0])
		{
			$scope.holdingTank.pop().setMap(null);
		}
		// $scope.infoWindows = [];
	}

	$scope.doListen = function() {
		$ionicLoading.show({
		    template: 'LISTENING...'
		});
		SpeechHandler.listen().then(function(results) {
			$ionicLoading.hide();
			console.log(results);
			if(results.indexOf("stadium") > 0) {
				SpeechHandler.say("Got it - using the route for Lucas Oil Stadium.")
				.then(function(){
					console.log("Back from saying things");
					// https://www.google.com/maps?saddr=My+Location&daddr=39.7601007,-86.1638877 
					GeoHandler.monitorProgress().then(function(){
						// Do things here if we need to
					});
				})
			}
			if(results.indexOf("launch") > 0) {
				SpeechHandler.say("Got it - using the route for Launch Fishers.")
				.then(function(){
					GeoHandler.monitorProgress().then(function(){
						// Do things here if we need to
					});
				})
			}
			if(results.indexOf("home") > 0) {
				SpeechHandler.say("Got it - using the route for home.")
				.then(function(){
					GeoHandler.monitorProgress().then(function(){
						// Do things here if we need to
					});
				})
			}
		});
	}


	$scope.$on('$ionicView.beforeEnter', function(){
		$scope.init();
	});
})
