angular.module('ConfigMaster', [])

.factory('ConfigMaster', function($rootScope) {
	var factory = {};

	// API Endpoint
	// factory.chavaweb_api = "http://172.20.10.7:8081/api/"
	// factory.chavaweb_api = "http://stage-api.askliv.com/api/"
	// factory.chavaweb_api = "http://127.0.0.1:8081/api/"
	// factory.chavaweb_api = "http://localhost:8181/api/"
	factory.chavaweb_api = "https://saferouteapi.azurewebsites.net/api/"

	// Local Storage Path Config
	factory.local_storageKey = "com.evilgrinstudios.saferoute";

	factory.appName = "Safe Route"

	// Crypto Config
	factory.crypto_sharedSecret = "sdjhfsdfsdp7urf908ewjdlsdf52sksdf";


	return factory;
});